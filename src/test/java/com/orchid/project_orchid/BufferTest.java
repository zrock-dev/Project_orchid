package com.orchid.project_orchid;

import com.orchid.project_orchid.model.*;
import com.orchid.project_orchid.server.model.Buffer;
import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.VisitorClient;
import com.orchid.project_orchid.user_profiles.filters.BasicServices;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BufferTest {
    @Test
    public void testBufferStorage(){
        Buffer buffer = new Buffer();
        VisitorClient priorityClient = new VisitorClient("b", UserType.PRIORITY, BasicServices.INTERNET);
        AccountHolderClient accountHolderClient = new AccountHolderClient("c","1", OperationType.DEPOSIT,1,1);
        AccountHolderClient accountHolderClient1 = new AccountHolderClient("d","2",BasicServices.ELECTRICITY);

        buffer.addFirstSelected(accountHolderClient);
        buffer.addFirstSelected(priorityClient);
        buffer.addFirstSelected(accountHolderClient1);

        assertEquals(3,buffer.getFirstSelected().size());
    }

    @Test
    public void testQueueFormat(){
        VisitorClient standardClient = new VisitorClient("a",UserType.STANDARD,BasicServices.GAS);
        AccountHolderClient accountHolderClient = new AccountHolderClient("b","1",OperationType.DEPOSIT,1,2);
        VisitorClient priorityClient = new VisitorClient("c",UserType.PRIORITY,BasicServices.INTERNET);
        AccountHolderClient accountHolderClient1 = new AccountHolderClient("d","2",BasicServices.INTERNET);
        VisitorClient priorityClient1 = new VisitorClient("e",UserType.PRIORITY,BasicServices.INTERNET);
        AccountHolderClient accountHolderClient2 = new AccountHolderClient("f","3",OperationType.WITHDRAW,1,2);
        VisitorClient priorityClient2 = new VisitorClient("g",UserType.PRIORITY,BasicServices.INTERNET);
        AccountHolderClient accountHolderClient3 = new AccountHolderClient("h","4",BasicServices.ELECTRICITY);
        VisitorClient priorityClient3 = new VisitorClient("i",UserType.PRIORITY,BasicServices.INTERNET);
        AccountHolderClient accountHolderClient4 = new AccountHolderClient("j","5",OperationType.LOAN_PAYMENT,1,3);
        VisitorClient priorityClient4 = new VisitorClient("k",UserType.PRIORITY,BasicServices.INTERNET);

        Buffer buffer1 = new Buffer();

        buffer1.addStandard(standardClient);
        buffer1.addFirstSelected(accountHolderClient);
        buffer1.addFirstSelected(priorityClient);
        buffer1.addFirstSelected(accountHolderClient1);
        buffer1.addFirstSelected(priorityClient1);
        buffer1.addFirstSelected(accountHolderClient2);
        buffer1.addFirstSelected(priorityClient2);
        buffer1.addFirstSelected(accountHolderClient3);
        buffer1.addFirstSelected(priorityClient3);
        buffer1.addFirstSelected(accountHolderClient4);
        buffer1.addFirstSelected(priorityClient4);

        assertEquals(accountHolderClient, buffer1.getFirstSelected().remove());
        assertEquals(priorityClient, buffer1.getFirstSelected().remove());
        assertEquals(accountHolderClient1, buffer1.getFirstSelected().remove());
        assertEquals(priorityClient1, buffer1.getFirstSelected().remove());
        assertEquals(accountHolderClient2, buffer1.getFirstSelected().remove());
        assertEquals(priorityClient2, buffer1.getFirstSelected().remove());
        assertEquals(accountHolderClient3, buffer1.getFirstSelected().remove());
        assertEquals(priorityClient3, buffer1.getFirstSelected().remove());
        assertEquals(accountHolderClient4, buffer1.getFirstSelected().remove());
        assertEquals(priorityClient4, buffer1.getFirstSelected().remove());
    }
}
