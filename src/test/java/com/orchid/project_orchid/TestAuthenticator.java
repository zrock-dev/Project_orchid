package com.orchid.project_orchid;

import com.orchid.project_orchid.server.controller.Authenticator;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestAuthenticator {
    @Test
    public void testAuthenticator(){
        boolean result;
        Authenticator authenticator = new Authenticator();
        result = authenticator.decryptPassword("Okay");
        assertTrue(result);
        result = authenticator.decryptPassword("okay");
        assertFalse(result);
        result = authenticator.decryptPassword("Coffee");
        assertFalse(result);
    }
}
