package com.orchid.project_orchid;

import com.orchid.project_orchid.client.model.Encryptor;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class TestEncryptor {
    @Test
    public void probabilityTest(){
        Encryptor encryptor = new Encryptor();
        ArrayList<String> keysOfTheClients = new ArrayList<>();

        int countOfKeyCorrect = 0;
        int countOfKeyFalse = 0;

        for (int i = 0; i < 10; i++) {
            String individualKey = encryptor.makePassword();
            keysOfTheClients.add(individualKey);
        }

        for (int i = 0; i < keysOfTheClients.size(); i++) {
            if ("Okay".equals(keysOfTheClients.get(i))) {
                countOfKeyCorrect++;
            } else{
                countOfKeyFalse++;
            }
        }

        int difference = countOfKeyCorrect - countOfKeyFalse;

        assertEquals(8, countOfKeyCorrect);
        assertEquals(2, countOfKeyFalse);
        assertEquals(6, difference);
    }
}