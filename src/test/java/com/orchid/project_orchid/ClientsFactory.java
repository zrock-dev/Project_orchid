package com.orchid.project_orchid;

import com.orchid.project_orchid.client.model.ClientGenerator;
import com.orchid.project_orchid.client.model.ClientStorage;
import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.clients.VisitorClient;
import com.orchid.project_orchid.user_profiles.filters.BasicServices;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;

import java.util.ArrayList;

public class ClientsFactory {
    private static ClientGenerator clientGenerator = new ClientGenerator(new ClientStorage());

    public static VisitorClient makeStandardVisitor(){
        return new VisitorClient(
                "3223",
                UserType.STANDARD,
                BasicServices.ELECTRICITY
        );
    }

    public static VisitorClient makePriorityVisitor(){
        return new VisitorClient(
                "3223",
                UserType.PRIORITY,
                BasicServices.GAS
        );
    }

    public static AccountHolderClient makeBasicAccountHolder(){
        return new AccountHolderClient(
                "3232",
                "bananas",
                BasicServices.INTERNET
        );
    }

    public static AccountHolderClient makeFinancialAccountHolder(){
        return new AccountHolderClient(
                "3232",
                "bananas_verdes",
                OperationType.values()[((int) (Math.random() * 2))],
                10,
                0
        );
    }

    public static ArrayList<Client> basicServiceDummyData() {
        int bigSize = 1000;
        ArrayList<Client> basicServiceClients = new ArrayList<>(bigSize);

        for (int j = 0; j < bigSize/2; j++) {
            basicServiceClients.add(clientGenerator.makeAVisitor());
            basicServiceClients.add(clientGenerator.makeAnAccountHolder(ServiceType.BASIC_SERVICE));
        }
        return basicServiceClients;
    }


    public static ArrayList<Client> basicServiceDummyData(int size) {
        ArrayList<Client> basicServiceClients = new ArrayList<>(size);

        for (int j = 0; j < size / 2; j++) {
            basicServiceClients.add(clientGenerator.makeAVisitor());
            basicServiceClients.add(clientGenerator.makeAnAccountHolder(ServiceType.BASIC_SERVICE));

        }
        return basicServiceClients;
    }

    public static ArrayList<AccountHolderClient> financialOperationsDummyData(){
        int bigSize = 1000;
        ArrayList<AccountHolderClient> clients = new ArrayList<>(bigSize);

        for (int j = 0; j < bigSize / 3; j++) {
            clients.add(clientGenerator.makeAnAccountHolder(ServiceType.FINANCIAL_OPERATION));
        }
        return clients;
    }
}
