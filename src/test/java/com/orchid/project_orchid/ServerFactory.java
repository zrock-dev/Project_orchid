package com.orchid.project_orchid;

import com.orchid.project_orchid.server.controller.Server;
import com.orchid.project_orchid.server.model.Buffer;
import com.orchid.project_orchid.server.model.ClientClassifier;

import java.io.IOException;

public class ServerFactory {

    public static Server bakeAServer(){
        Server server = new Server(new ClientClassifier(new Buffer()));

        Thread serverThread = new Thread(
                () -> {
                    try {
                        server.startServer();
                    } catch (IOException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
        );
        serverThread.setName("Server Thread");
        serverThread.start();
        return server;
    }
}
