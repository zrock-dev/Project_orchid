package com.orchid.project_orchid;

import com.orchid.project_orchid.client.controller.Feeder;
import com.orchid.project_orchid.server.controller.Server;
import com.orchid.project_orchid.user_profiles.clients.Client;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class FeederTest {
    static Server server;

    @BeforeAll
    static void startServer() {
        server = ServerFactory.bakeAServer();
    }

    @AfterAll
    static void closeServer() {
        server.stopServer();
    }

    @Test
    public void simpleDataTransfer() {
        Feeder client = new Feeder();
        ArrayList<Client> clients = ClientsFactory.basicServiceDummyData();

        client.connectToServer("127.0.0.1", 6666);
        client.load(clients);
        client.beginTransference();
    }

    @Test
    void multipleTransferAtDifferentRates() {
        Feeder client = new Feeder();
        client.load(
                ClientsFactory.basicServiceDummyData()
        );

        // Loop 10 times to verify successful connections.
        for (int i = 0; i < 10; i++) {
            ArrayList<Client> clients = ClientsFactory.basicServiceDummyData(10 * i + 1);

            // Send items to the server
            client.connectToServer("127.0.0.1", 6666);
            client.load(clients);
            client.beginTransference();

            // Wait to confirm the server is still open.
            try {
                TimeUnit.MILLISECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
