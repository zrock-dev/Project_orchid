package com.orchid.project_orchid.model;

import com.orchid.project_orchid.ClientsFactory;
import com.orchid.project_orchid.client.model.ClientStorage;
import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.orchid.project_orchid.ClientsFactory.basicServiceDummyData;
import static com.orchid.project_orchid.ClientsFactory.financialOperationsDummyData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ClientStorageTest {

    @Test
    void happyPath() {
        ClientStorage clientStorage = new ClientStorage();
        ArrayList<Client> clients = new ArrayList<>(600);

        // Filling lists
        for (int i = 0; i < 100; i++) {
            clients.add(ClientsFactory.makeStandardVisitor());
            clients.add(ClientsFactory.makePriorityVisitor());
            clients.add(ClientsFactory.makeBasicAccountHolder());
        }

        for (int i = 0; i < 300; i++) {
            clients.add(ClientsFactory.makeFinancialAccountHolder());
        }

        // actual test
        for (Client client :
                clients) {
            clientStorage.add(client);
        }

        assertEquals(clients.size(), clientStorage.size());
    }

    @Test
    void saveAllKindOfUsers() {
        int bigSize = 10000;
        int totalSize = 0;

        ArrayList<Client> standardClients = new ArrayList<>(bigSize);
        ArrayList<Client> priorityClients = new ArrayList<>(bigSize);
        ArrayList<Client> accountHolderClients = new ArrayList<>(bigSize);
        ArrayList<AccountHolderClient> financialOperationClients = new ArrayList<>(bigSize * 3);

        for (int i = 0; i < bigSize; i++) {
            standardClients.add(ClientsFactory.makeStandardVisitor());
        }
        totalSize += standardClients.size();

        for (int i = 0; i < bigSize; i++) {
            priorityClients.add(ClientsFactory.makePriorityVisitor());
        }
        totalSize += priorityClients.size();


        for (int i = 0; i < bigSize; i++) {
            accountHolderClients.add(ClientsFactory.makeBasicAccountHolder());
        }
        totalSize += accountHolderClients.size();

        for (int i = 0; i < bigSize * 3; i++) {
            financialOperationClients.add(ClientsFactory.makeFinancialAccountHolder());
        }
        totalSize += financialOperationClients.size();

        List<AccountHolderClient> depositClients = financialOperationClients.stream()
                .filter(client -> client.getFinancialOperation().equals(OperationType.DEPOSIT))
                .collect(Collectors.toList());

        List<AccountHolderClient> withdrawClients = financialOperationClients.stream()
                .filter(client -> client.getFinancialOperation().equals(OperationType.WITHDRAW))
                .collect(Collectors.toList());

        List<AccountHolderClient> loanPaymentClients = financialOperationClients.stream()
                .filter(client -> client.getFinancialOperation().equals(OperationType.LOAN_PAYMENT))
                .collect(Collectors.toList());

        ClientStorage clientStorage = new ClientStorage();
        for (Client client :
                standardClients) {
            clientStorage.add(client);
        }

        for (Client client :
                priorityClients) {
            clientStorage.add(client);
        }

        for (Client client :
                accountHolderClients) {
            clientStorage.add(client);
        }

        for (Client client :
                depositClients) {
            clientStorage.add(client);
        }

        for (Client client :
                withdrawClients) {
            clientStorage.add(client);
        }

        for (Client client :
                loanPaymentClients) {
            clientStorage.add(client);
        }

        assertEquals(totalSize, clientStorage.size());
    }

    @Test
    void basicServicesPoll(){
        ArrayList<Client> basicServices = basicServiceDummyData();

        ClientStorage clientStorage = new ClientStorage();
        for (Client client :
                basicServices) {
            clientStorage.add(client);
        }

        for (UserType operation:
                UserType.values()) {

            List<Client> accountHolderClients = basicServices.stream()
                    .filter(client -> client.getUserType().equals(operation))
                    .collect(Collectors.toList());

            ArrayList<Client> accountHolderPoll = clientStorage.pollByFilter(
                    ServiceType.BASIC_SERVICE,
                    operation,
                    accountHolderClients.size()
            );

            for (int i = 0; i < accountHolderClients.size(); i++) {
                assertTrue(accountHolderClients.contains(
                                accountHolderPoll.get(i)
                        )
                );
            }
        }
    }

    @Test
    void financialOperationsPoll(){
        ArrayList<AccountHolderClient> operationsList = financialOperationsDummyData();

        ClientStorage clientStorage = new ClientStorage();
        for (Client client :
                operationsList) {
            clientStorage.add(client);
        }

        for (OperationType operation:
             OperationType.values()) {

            List<AccountHolderClient> accountHolderClients = operationsList.stream()
                    .filter(client -> client.getFinancialOperation().equals(operation))
                    .collect(Collectors.toList());

            ArrayList<Client> accountHolderPoll = clientStorage.pollByFilter(
                    ServiceType.FINANCIAL_OPERATION,
                    operation,
                    accountHolderClients.size()
            );

            for (int i = 0; i < accountHolderClients.size(); i++) {
                assertTrue(accountHolderClients.contains(
                                accountHolderPoll.get(i)
                        )
                );
            }
        }
    }

}