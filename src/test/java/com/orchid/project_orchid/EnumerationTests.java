package com.orchid.project_orchid;

import com.orchid.project_orchid.user_profiles.filters.BasicServices;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EnumerationTests {

    @Test
    void userType(){
        assertEquals("S", UserType.STANDARD.code());
        assertEquals("P", UserType.PRIORITY.code());
        assertEquals("A", UserType.ACCOUNT_HOLDER.code());
    }

    @Test
    void basicServices(){
        assertEquals("G", BasicServices.GAS.code());
        assertEquals("E", BasicServices.ELECTRICITY.code());
        assertEquals("I", BasicServices.INTERNET.code());
    }

    @Test
    void operationType(){
        assertEquals("W", OperationType.WITHDRAW.code());
        assertEquals("D", OperationType.DEPOSIT.code());
        assertEquals("L", OperationType.LOAN_PAYMENT.code());
    }

    @Test
    void serviceType(){
        assertEquals("B", ServiceType.BASIC_SERVICE.code());
        assertEquals("F", ServiceType.FINANCIAL_OPERATION.code());
    }
}
