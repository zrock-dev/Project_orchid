package com.orchid.project_orchid.controller;

import com.orchid.project_orchid.ClientsFactory;
import com.orchid.project_orchid.ServerFactory;
import com.orchid.project_orchid.client.controller.Request;
import com.orchid.project_orchid.client.model.ClientStorage;
import com.orchid.project_orchid.server.controller.Server;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class RequestTest {
    static Server server;
    @BeforeAll
    static void startServer(){
        server = ServerFactory.bakeAServer();
    }

    @Test
    void happyPath(){
        ClientStorage clientStorage = new ClientStorage();
        ArrayList<Client> dummyData = ClientsFactory.basicServiceDummyData();
        for (Client client:
             dummyData) {
            clientStorage.add(client);
        }

        Request request = new Request(clientStorage);
        request.users(UserType.ACCOUNT_HOLDER, 200);
        request.processOrder();
    }

    @AfterAll
    static void closeServer(){
        server.stopServer();
    }
}