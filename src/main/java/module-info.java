module com.orchid.project_orchid {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.ikonli.javafx;

    exports com.orchid.project_orchid;
    opens com.orchid.project_orchid to javafx.fxml;

    exports com.orchid.project_orchid.server.view;
    opens com.orchid.project_orchid.server.view to javafx.base;
    opens com.orchid.project_orchid.server.view.rows to javafx.base;
    opens com.orchid.project_orchid.server.view.tables to javafx.base;

    exports com.orchid.project_orchid.server;
    opens com.orchid.project_orchid.server to javafx.fxml;

    exports com.orchid.project_orchid.user_profiles.clients;
    exports com.orchid.project_orchid.user_profiles.filters;
    exports com.orchid.project_orchid.client.controller;
    opens com.orchid.project_orchid.client.controller to javafx.fxml;
    exports com.orchid.project_orchid.client;
    opens com.orchid.project_orchid.client to javafx.fxml;

}
