package com.orchid.project_orchid.user_profiles.clients;

import com.orchid.project_orchid.user_profiles.filters.BasicServices;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;

public class AccountHolderClient extends Client {
    // Personal
    private final String id;

    // Financial operations
    private final String password;
    private int balance;
    private final int transaction;

    // Operation classification
    private final OperationType operation;
    private final BasicServices basicService;
    public final static int MINIMAL_ACCOUNT_BALANCE = 50;

    public AccountHolderClient(String uniqueID, String password, OperationType operation,
                               int balance, int transaction) {

        super(UserType.ACCOUNT_HOLDER, ServiceType.FINANCIAL_OPERATION);
        this.operation = operation;
        this.password = password;
        this.balance = balance;
        this.transaction = transaction;
        this.basicService = null;

        // Own ID
        id = super.getId() + operation.code() + uniqueID;
    }

    public AccountHolderClient(String uniqueID, String password, BasicServices basicService) {

        super(UserType.ACCOUNT_HOLDER, ServiceType.BASIC_SERVICE);
        this.password = password;
        this.basicService = basicService;

        // enforced initializations
        transaction = 0;
        balance = MINIMAL_ACCOUNT_BALANCE;
        operation = null;

        // Own id
        id = super.getId() + basicService.code() + uniqueID;
    }

    public OperationType getFinancialOperation() {return operation;}

    public int getBalance() {
        return balance;
    }

    public int getTransaction() {
        return transaction;
    }

    public String getPassword() {
        return password;
    }

    public void updateBalance(int amount){
        balance = amount;
    }

    @Override
    public BasicServices getBasicService() {
        return basicService;
    }

    @Override
    public String getId() {
        return id;
    }
}
