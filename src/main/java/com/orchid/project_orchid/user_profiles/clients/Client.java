package com.orchid.project_orchid.user_profiles.clients;

import com.orchid.project_orchid.user_profiles.filters.BasicServices;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;

import java.io.Serializable;

public abstract class Client implements Serializable {
    private final UserType userType;
    private final ServiceType serviceType;
    private final String id;

    public Client(UserType userType, ServiceType serviceType) {
        this.userType = userType;
        this.serviceType = serviceType;
        id = userType.code() + serviceType.code();
    }

    public String getId(){
        return id;
    }

    public UserType getUserType() {
        return userType;
    }

    public ServiceType getServiceType() {return serviceType;}

    /**
     * The abstract method getBasicService ensures that every client
     * has a basic service.
     * @return A basic service.
     */
    public abstract BasicServices getBasicService();

    /**
     * For the clients that want to use the basic services of the bank
     * @return false if the client doesn't require a service.
     */
    public boolean requiresBasicServices(){
        return serviceType.equals(ServiceType.BASIC_SERVICE);
    }
}
