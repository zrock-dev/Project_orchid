package com.orchid.project_orchid.user_profiles.clients;

import com.orchid.project_orchid.user_profiles.filters.BasicServices;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;

public class VisitorClient extends Client {
    private final BasicServices service;
    private final String id;

    public VisitorClient(String uniqueID, UserType userType, BasicServices service) {
        super(userType, ServiceType.BASIC_SERVICE);
        this.service = service;
        id = super.getId() + service.code() + uniqueID;
    }

    @Override
    public BasicServices getBasicService() {
        return service;
    }

    @Override
    public String getId() {
        return id;
    }
}
