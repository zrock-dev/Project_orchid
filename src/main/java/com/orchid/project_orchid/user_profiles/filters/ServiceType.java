package com.orchid.project_orchid.user_profiles.filters;

/**
 * The ServiceType enum lists the type of services offered by the bank.
 * <p>
 * Such as: Basic services and financial operations.
 */
public enum ServiceType {
    BASIC_SERVICE("B"),
    FINANCIAL_OPERATION("F");

    private final String character;

    ServiceType(String character){
        this.character = character;
    }

    public String code(){
        return character;
    }
}
