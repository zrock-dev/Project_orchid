package com.orchid.project_orchid.user_profiles.filters;

/**
 * The OperationType enum holds the kind of operations that
 * an account holder client can perform such as:
 * <p>
 * Withdraw, Deposit or make a Loan Payment
 */
public enum OperationType {
    WITHDRAW("W"),
    DEPOSIT("D"),
    LOAN_PAYMENT("L");

    private final String character;

    OperationType(String character){
        this.character = character;
    }

    public String code(){
        return character;
    }
}
