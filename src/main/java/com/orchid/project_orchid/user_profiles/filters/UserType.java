package com.orchid.project_orchid.user_profiles.filters;

/**
 * The UserType enum classifies the bank clients according its type, they can be
 * a visitor or an account holder. The visitors are classified according to the priority
 * standard or normal, and priority for user that need to be attended faster.
 */
public enum UserType {
    PRIORITY("P"),
    STANDARD("S"),
    ACCOUNT_HOLDER("A");

    private final String character;

    UserType(String character){
        this.character = character;
    }

    public String code(){
        return character;
    }
}
