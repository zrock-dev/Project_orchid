package com.orchid.project_orchid.user_profiles.filters;

/**
 * In the BasicServices enum we can find, operations that can be
 * performed by visitors and account holders as well.
 * <p>
 * We have: Electricity, Gas, and Internet.
 */
public enum BasicServices {
    ELECTRICITY("E"),
    GAS("G"),
    INTERNET("I");

    private final String character;

    BasicServices(String character){
        this.character = character;
    }

    public String code(){
        return character;
    }
}
