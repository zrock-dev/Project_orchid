package com.orchid.project_orchid.server.view.rows;

public class BasicServiceRow {
    private String id;
    private String userType;
    private String serviceType;
    private String serviceName;
    private int balance;

    public void setData(String id, String userType, String serviceType, String serviceName, int balance) {
        this.id = id;
        this.userType = userType;
        this.serviceType = serviceType;
        this.serviceName = serviceName;
        this.balance = balance;
    }

    public void setData(String id, String userType, String serviceType, String serviceName) {
        this.id = id;
        this.userType = userType;
        this.serviceType = serviceType;
        this.serviceName = serviceName;
    }

    public String getId() {
        return id;
    }

    public String getUserType() {
        return userType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public int getBalance() {
        return balance;
    }

    public String getServiceName() {
        return serviceName;
    }
}
