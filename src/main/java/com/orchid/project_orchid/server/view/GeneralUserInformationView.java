package com.orchid.project_orchid.server.view;

import com.orchid.project_orchid.server.controller.Teller;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import javafx.application.Platform;
import javafx.scene.control.Label;

public class GeneralUserInformationView {
    // Labels
    private Label queueSizeLabel;
    private Label turnDurationLabel;
    private Label nextUserLabel;

    private String nextUserWaiting;

    public GeneralUserInformationView(Label queueSizeLabel, Label turnDurationLabel, Label nextUserLabel) {
        this.queueSizeLabel = queueSizeLabel;
        this.turnDurationLabel = turnDurationLabel;
        this.nextUserLabel = nextUserLabel;
        updateTurnDuration();
    }

    public void updateQueueSize(int size){
        Platform.runLater(() -> queueSizeLabel.setText(String.valueOf(size)));
    }

    public void updateNextUserWaiting(String id, UserType type){
        Platform.runLater(() ->  nextUserLabel.setText(id + " " + type.name()));
    }

    public void updateNextUserWaiting(String id){
        Platform.runLater(() ->  nextUserLabel.setText(id + " "));
    }

    private void updateTurnDuration(){
        turnDurationLabel.setText(String.valueOf(Teller.TURN_DURATION));
    }

}

