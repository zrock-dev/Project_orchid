package com.orchid.project_orchid.server.view.rows;

public class FinancialOperationRow {
    private String id;
    private int balance;
    private int transaction;
    private String operationType;
    private String status;

    public void setData(String id, int balance, String type, int transaction, String status) {
        this.id = id;
        this.balance = balance;
        this.operationType = type;
        this.transaction = transaction;
        this.status = status;
    }

    public void setData(String id, int balance, String type, int transaction) {
        this.id = id;
        this.balance = balance;
        this.operationType = type;
        this.transaction = transaction;
    }

    public String getId() {
        return id;
    }

    public int getBalance() {
        return balance;
    }

    public String getOperationType() {
        return operationType;
    }

    public int getTransaction() {
        return transaction;
    }

    public String getStatus() {
        return status;
    }
}
