package com.orchid.project_orchid.server.view;

import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.server.view.tables.BasicServiceTable;
import com.orchid.project_orchid.server.view.tables.FinancialOperationTable;
import javafx.application.Platform;
import javafx.scene.layout.AnchorPane;

public class ClientOnTellerView {
    private AnchorPane anchor;
    private BasicServiceTable basicServiceTable;
    private FinancialOperationTable financialOperationTable;

    public ClientOnTellerView(AnchorPane anchor) {
        basicServiceTable = new BasicServiceTable();
        financialOperationTable = new FinancialOperationTable();

        anchor.setMaxSize(870, 138);
        this.anchor = anchor;
    }

    public void update(Client client){
        Platform.runLater(() -> {
            anchor.getChildren().clear();
            if (client.requiresBasicServices()){
                anchor.getChildren().add(
                        basicServiceTable.updateTable(client)
                );
            }else {
                anchor.getChildren().add(
                        financialOperationTable.update(((AccountHolderClient) client))
                );
            }
        });
    }
}
