package com.orchid.project_orchid.server.view.exceptions;

public class InvalidClientException extends RuntimeException{
    public InvalidClientException(String message) {
        super(message);
    }
}
