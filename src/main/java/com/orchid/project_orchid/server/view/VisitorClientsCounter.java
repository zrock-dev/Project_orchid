package com.orchid.project_orchid.server.view;

import com.orchid.project_orchid.user_profiles.filters.UserType;
import javafx.application.Platform;
import javafx.scene.control.Label;

public class VisitorClientsCounter {

    // labels
    private Label priorityClientsLabel;
    private Label standardClientsLabel;
    private Label accountHolderClientsLabel;

    private int countStandard;
    private int countPriority;
    private int countAccountHolder;

    public VisitorClientsCounter(Label priorityClientsLabel, Label standardClientsLabel, Label accountHolderClientsLabel) {
        this.priorityClientsLabel = priorityClientsLabel;
        this.standardClientsLabel = standardClientsLabel;
        this.accountHolderClientsLabel = accountHolderClientsLabel;

        countStandard = 0;
        countPriority = 0;
        countAccountHolder = 0;
    }

    public void count(UserType type){
        Platform.runLater(() -> {
            if (UserType.ACCOUNT_HOLDER == type){
                countAccountHolder++;
                accountHolderClientsLabel.setText(String.valueOf(countAccountHolder));
            } else if (UserType.PRIORITY == type){
                countPriority++;
                priorityClientsLabel.setText(String.valueOf(countPriority));
            }else {
                countStandard++;
                standardClientsLabel.setText(String.valueOf(countStandard));
            }
        });
    }
}

