package com.orchid.project_orchid.server.view.tables;

import com.orchid.project_orchid.server.controller.OperationStatus;
import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.server.view.rows.FinancialOperationRow;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class FinancialOperationTable {
    private FinancialOperationRow row;
    private TableView<FinancialOperationRow> table;

    public FinancialOperationTable(){
        table = new TableView<>();
        loadColumns();
        row = new FinancialOperationRow();
        table.setMinSize(870, 138);
        table.setMaxHeight(138);
    }

    public FinancialOperationTable(String onetime){
        table = new TableView<>();
        loadColumns();
        row = new FinancialOperationRow();
        table.setMinSize(870, 362);
        table.getColumns().add(statusColumn());
    }

    private void loadColumns(){
        TableColumn<FinancialOperationRow, String> colIdentifier = new TableColumn<>("Identifier");;
        TableColumn<FinancialOperationRow, Integer> colBalance = new TableColumn<>("Balance");;
        TableColumn<FinancialOperationRow, String> colOperationType = new TableColumn<>("Operation Type");;
        TableColumn<FinancialOperationRow, Integer> colTransaction = new TableColumn<>("Transaction");;

        colIdentifier.setCellValueFactory(new PropertyValueFactory<>("id"));
        colBalance.setCellValueFactory(new PropertyValueFactory<>("balance"));
        colOperationType.setCellValueFactory(new PropertyValueFactory<>("operationType"));
        colTransaction.setCellValueFactory(new PropertyValueFactory<>("transaction"));

        table.getColumns().addAll(colIdentifier, colBalance, colOperationType, colTransaction);

        for (TableColumn<?, ?> column:
                table.getColumns()) {
            column.setPrefWidth(150);
        }
    }

    public TableView<FinancialOperationRow> update(AccountHolderClient client){
        table.getItems().clear();
        row.setData(
                client.getId(),
                client.getBalance(),
                client.getFinancialOperation().name(),
                client.getTransaction()
        );
        table.getItems().add(row);
        return table;
    }

    public TableView<FinancialOperationRow> getTable() {
        return table;
    }

    public void record(AccountHolderClient client, OperationStatus status){
        row.setData(
                client.getId(),
                client.getBalance(),
                client.getFinancialOperation().name(),
                client.getTransaction(),
                status.name()
        );
        table.getItems().add(row);
    }

    private TableColumn<FinancialOperationRow, String> statusColumn(){
        TableColumn<FinancialOperationRow, String> colStatus = new TableColumn<>("Status");
        colStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        return colStatus;
    }
}
