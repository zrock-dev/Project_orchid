package com.orchid.project_orchid.server.view.tables;

import com.orchid.project_orchid.server.view.rows.BasicServiceRow;
import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class BasicServiceTable {
    private final TableView<BasicServiceRow> table;
    private TableColumn<BasicServiceRow, Integer> balance;
    private final BasicServiceRow row;

    public BasicServiceTable() {
        table = new TableView<>();
        loadColumns();
        row = new BasicServiceRow();

        table.setMinSize(870, 138);
        table.setMaxHeight(138);
    }

    private void loadColumns(){
        // create columns
        TableColumn<BasicServiceRow, String> id = new TableColumn<>("Identifier");
        TableColumn<BasicServiceRow, String> userType = new TableColumn<>("User Type");
        TableColumn<BasicServiceRow, String> serviceType = new TableColumn<>("Service Type");
        TableColumn<BasicServiceRow, String> filter = new TableColumn<>("Service");
        balance = new TableColumn<>("Balance");

        // associate values
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        userType.setCellValueFactory(new PropertyValueFactory<>("userType"));
        serviceType.setCellValueFactory(new PropertyValueFactory<>("serviceType"));
        filter.setCellValueFactory(new PropertyValueFactory<>("serviceName"));
        balance.setCellValueFactory(new PropertyValueFactory<>("balance"));

        table.getColumns().addAll(id, userType, serviceType, filter, balance);
        table.getItems().add(new BasicServiceRow());

        for (TableColumn<?, ?> column :
                table.getColumns()) {
            column.setPrefWidth(150);
        }
    }

    public TableView<BasicServiceRow> updateTable(Client client){
        table.getItems().clear();
        if (client.getUserType() == UserType.ACCOUNT_HOLDER){
            balance.setVisible(true);
            AccountHolderClient holderClient = ((AccountHolderClient) client);
            row.setData(holderClient.getId(),
                    holderClient.getUserType().name(),
                    holderClient.getServiceType().name(),
                    holderClient.getBasicService().name(),
                    holderClient.getBalance()
            );
        }else {
            balance.setVisible(false);
            row.setData(client.getId(),
                    client.getUserType().name(),
                    client.getServiceType().name(),
                    client.getBasicService().name()
            );
        }
        table.getItems().add(row);
        return table;
    }
}
