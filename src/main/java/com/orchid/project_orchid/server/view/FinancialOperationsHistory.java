package com.orchid.project_orchid.server.view;

import com.orchid.project_orchid.server.controller.OperationStatus;
import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.server.view.tables.FinancialOperationTable;
import javafx.scene.layout.AnchorPane;

public class FinancialOperationsHistory {
    private final FinancialOperationTable table;

    public FinancialOperationsHistory(AnchorPane tableSpace) {
        table = new FinancialOperationTable("applebee");
        tableSpace.setMinSize(800, 300);
        tableSpace.getChildren().add(table.getTable());
    }

    public void record(AccountHolderClient client, OperationStatus status){
        table.record(client, status);
    }
}
