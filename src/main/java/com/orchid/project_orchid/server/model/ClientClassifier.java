package com.orchid.project_orchid.server.model;

import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.filters.UserType;

public class ClientClassifier {
    private final Buffer buffer;

    public ClientClassifier(Buffer buffer) {
        this.buffer = buffer;
    }

    public void sortClient(Client client){
        if (client.getUserType().equals(UserType.STANDARD)){
            buffer.addStandard(client);
        }else {
            buffer.addFirstSelected(client);
        }
    }
}
