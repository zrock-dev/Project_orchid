package com.orchid.project_orchid.server.model;

import com.orchid.project_orchid.user_profiles.clients.Client;
import java.util.LinkedList;
import java.util.Queue;

public class Buffer {
    private Queue<Client> firstSelected;
    private Queue<Client> visitors;

    public Buffer() {
        this.firstSelected = new LinkedList<>();
        this.visitors = new LinkedList<>();
    }

    public void addFirstSelected(Client client){
        firstSelected.add(client);
    }

    public void addStandard(Client client){
        visitors.add(client);
    }

    public Queue<Client> getFirstSelected() {
        return firstSelected;
    }

    public Queue<Client> getVisitors() {
        return visitors;
    }

    public int getSizeFirstSelected() {
        return firstSelected.size();
    }

    public int getSizeStandard() {
        return visitors.size();
    }

    public boolean isFirstSelectedQueueEmpty (){
        return firstSelected.size() == 0;
    }

    public boolean isStandardQueueEmpty (){
        return visitors.size() == 0;
    }

    public boolean isEmpty(){
        return size() <= 0;
    }

    public Client peek(){
        if (!firstSelected.isEmpty()){
            return firstSelected.peek();
        }else {
            return visitors.peek();
        }
    }

    public int size(){
        return firstSelected.size() + visitors.size();
    }
}
