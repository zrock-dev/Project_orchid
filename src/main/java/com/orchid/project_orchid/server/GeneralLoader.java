package com.orchid.project_orchid.server;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class GeneralLoader {
    // General information
    @FXML
    private Label usersWaiting;
    @FXML
    private Label turnDuration;
    @FXML
    private Label nextUser;

    // attend users count
    @FXML
    private Label countPriority;
    @FXML
    private Label countStandard;
    @FXML
    private Label countAccountHolder;

    // user using the teller
    @FXML
    private AnchorPane currentUserOnTeller;

    // attended users history
    @FXML
    private AnchorPane financialAnchorPane;

    // To obtain labels
    public AnchorPane getFinancialAnchorPane(){
        return financialAnchorPane;
    }

    public AnchorPane getCurrentUserOnTeller() {
        return currentUserOnTeller;
    }

    public Label[] getGeneralLabels(){
        return new Label[]{usersWaiting, turnDuration, nextUser};
    }

    public Label[] getUsersCounterLabels(){
        return new Label[]{countPriority, countStandard, countAccountHolder};
    }

    @FXML
    void closeServer() {System.exit(0);}
}
