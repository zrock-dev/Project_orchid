package com.orchid.project_orchid.server.controller;

import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.server.view.*;

import java.util.concurrent.TimeUnit;

public class Teller {
    public final static int TURN_DURATION = 2;

    // view
    private final FinancialOperationsHistory clientsOperationHistory;
    private final ClientOnTellerView clientOnTellerView;
    private final VisitorClientsCounter visitorClientsCounter;

    public Teller(FinancialOperationsHistory clientsOperationHistory, ClientOnTellerView clientOnTellerView,
                  VisitorClientsCounter visitorClientsCounter) {

        this.clientsOperationHistory = clientsOperationHistory;
        this.clientOnTellerView = clientOnTellerView;
        this.visitorClientsCounter = visitorClientsCounter;
    }

    public void attendClient(Client client) {
        System.out.println(client.getId() + " Hit");
        clientOnTellerView.update(client);

        if (client.requiresBasicServices()){
            visitorClientsCounter.count(client.getUserType());
        }else {
            AccountHolderClient accountHolderClient = ((AccountHolderClient) client);

            OperationStatus status = processAccountHolder(accountHolderClient);
            clientsOperationHistory.record(accountHolderClient, status);
        }

        try {
            TimeUnit.SECONDS.sleep(TURN_DURATION);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private OperationStatus processAccountHolder(AccountHolderClient client){
        int clientBalance = client.getBalance();
        int transaction = client.getTransaction();
        OperationStatus status = OperationStatus.REJECTED;

        if (clientBalance < AccountHolderClient.MINIMAL_ACCOUNT_BALANCE){
            return status;
        }

        OperationType operation = client.getFinancialOperation();
        if (operation == OperationType.DEPOSIT){
            client.updateBalance(clientBalance + transaction);
            status = OperationStatus.ACCEPTED;

        }else if (operation == OperationType.WITHDRAW || operation == OperationType.LOAN_PAYMENT){
            if (clientBalance > transaction){
                client.updateBalance(clientBalance - transaction);
                status = OperationStatus.ACCEPTED;
            }
        }

        return status;
    }
}
