package com.orchid.project_orchid.server.controller;

public enum OperationStatus {
    REJECTED,
    ACCEPTED
}
