package com.orchid.project_orchid.server.controller.exceptions;

public class IllegalFilterException extends RuntimeException{
    public IllegalFilterException(String message) {
        super(message);
    }
}
