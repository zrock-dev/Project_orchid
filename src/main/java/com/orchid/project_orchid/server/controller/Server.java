package com.orchid.project_orchid.server.controller;

import com.orchid.project_orchid.server.model.ClientClassifier;
import com.orchid.project_orchid.user_profiles.clients.Client;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server {
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private ObjectInputStream in;
    private final ClientClassifier clientClassifier;

    public Server(ClientClassifier clientClassifier) {
        this.clientClassifier = clientClassifier;
    }

    public void startServer() throws IOException, ClassNotFoundException {
        serverSocket = new ServerSocket(6666);

        while (true) {
            try {
                clientSocket = serverSocket.accept();
                in = new ObjectInputStream(clientSocket.getInputStream());
                receiveClients();
            }catch (SocketException ignored){
                break;
            }
        }
    }

    private void receiveClients() throws IOException, ClassNotFoundException {
        while (clientSocket.isConnected()) {
            try {
                clientClassifier.sortClient(
                        ((Client) in.readObject())
                );
            }catch (EOFException ignored){
                break;
            }catch (StreamCorruptedException e){
                e.printStackTrace();
            }
        }
    }

    public void stopServer(){
        try {
            in.close();
            clientSocket.close();
            serverSocket.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
