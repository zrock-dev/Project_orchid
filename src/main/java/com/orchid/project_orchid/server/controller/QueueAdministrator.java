package com.orchid.project_orchid.server.controller;

import com.orchid.project_orchid.server.model.Buffer;
import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import com.orchid.project_orchid.server.view.GeneralUserInformationView;
import java.util.concurrent.TimeUnit;

public class QueueAdministrator {
    private final Authenticator authenticator;
    private final GeneralUserInformationView generalUserInformationView;
    private final Buffer buffer;
    private final Teller teller;

    private int priorityClientsAttended;

    public QueueAdministrator(Buffer buffer, Teller teller, GeneralUserInformationView generalUserInformationView){
        authenticator = new Authenticator();
        priorityClientsAttended = 0;
        this.teller = teller;
        this.buffer = buffer;
        this.generalUserInformationView = generalUserInformationView;
    }

    public void manageBuffer() {
        while (true){
            while (!buffer.isEmpty()){
                generalUserInformationView.updateQueueSize(buffer.size());
                Client client = pickClient();
                if (client != null){
                    teller.attendClient(client);
                }else {
                    System.out.println("Dead user");
                }
            }

            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean authenticate (AccountHolderClient accountHolderClient){
        return authenticator.decryptPassword(accountHolderClient.getPassword());
    }

    public Client pickClient() {
        Client client = null;
        if (!buffer.isFirstSelectedQueueEmpty()) {
            client = buffer.getFirstSelected().remove();
            updateGeneralView();

            if (client.getUserType().equals(UserType.ACCOUNT_HOLDER)) {

                if (!authenticate((AccountHolderClient) client)) {
                    client = buffer.getFirstSelected().peek();
                    pickClient();
                } else {
                    priorityClientsAttended++;
                }

            }else {
                priorityClientsAttended++;
            }

            checkStandardQueue();
        } else if (!buffer.isStandardQueueEmpty()){
            client = buffer.getVisitors().remove();
            updateGeneralView();
        }

        return client;
    }

    private void updateGeneralView(){
        Client next = buffer.peek();
        if (next != null){
            generalUserInformationView.updateNextUserWaiting(next.getId(), next.getUserType());
        }else {
            generalUserInformationView.updateNextUserWaiting("Empty");
        }
    }

    private void checkStandardQueue() {
        if (priorityClientsAttended == 10){
            if (buffer.getVisitors().size() > 0){
                buffer.addFirstSelected(buffer.getVisitors().remove());
                priorityClientsAttended = 0;
            }
        }
    }
}
