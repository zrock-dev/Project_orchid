package com.orchid.project_orchid.server;

import com.orchid.project_orchid.server.controller.QueueAdministrator;
import com.orchid.project_orchid.server.controller.Server;
import com.orchid.project_orchid.server.controller.Teller;
import com.orchid.project_orchid.server.model.Buffer;
import com.orchid.project_orchid.server.model.ClientClassifier;
import com.orchid.project_orchid.server.view.ClientOnTellerView;
import com.orchid.project_orchid.server.view.FinancialOperationsHistory;
import com.orchid.project_orchid.server.view.GeneralUserInformationView;
import com.orchid.project_orchid.server.view.VisitorClientsCounter;
import com.orchid.project_orchid.utils.FileLoader;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;

public class ServerApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(FileLoader.getFile("server-side.fxml"));
        Scene scene = new Scene(loader.load());

        GeneralLoader gLoader = loader.getController();
        FinancialOperationsHistory financialOperationsHistory = new FinancialOperationsHistory(gLoader.getFinancialAnchorPane());
        ClientOnTellerView clientOnTellerView = new ClientOnTellerView(gLoader.getCurrentUserOnTeller());

        Label[] labels = gLoader.getUsersCounterLabels();
        VisitorClientsCounter clientsCounter = new VisitorClientsCounter(labels[0], labels[1], labels[2]);

        labels = gLoader.getGeneralLabels();
        GeneralUserInformationView generalUserInformationView = new GeneralUserInformationView(labels[0], labels[1], labels[2]);

        Buffer buffer = new Buffer();
        Teller teller = new Teller(financialOperationsHistory, clientOnTellerView, clientsCounter);
        QueueAdministrator administrator = new QueueAdministrator(buffer, teller, generalUserInformationView);

        Thread tellerThread = new Thread(administrator::manageBuffer);
        tellerThread.setName("Teller Thread");
        tellerThread.start();

        ClientClassifier classifier = new ClientClassifier(buffer);
        Server server = new Server(classifier);
        Thread serverThread = new Thread(() -> {
            try {
                server.startServer();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });

        serverThread.setName("Server Thread");
        serverThread.start();

        primaryStage.setTitle("Server");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
