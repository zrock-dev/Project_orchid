package com.orchid.project_orchid.client.controller;

import com.orchid.project_orchid.client.model.ClientStorage;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;

import java.util.HashMap;
import java.util.Map;

public class Request {
    private final Map<Object, Integer> financialOperationRequests;
    private final Map<Object, Integer> basicRequests;
    private final ClientStorage clientStorage;
    private final Feeder feeder;

    public Request(ClientStorage clientStorage) {
        this.clientStorage = clientStorage;
        financialOperationRequests = new HashMap<>();
        basicRequests = new HashMap<>();
        feeder = new Feeder();
    }

    public void users(UserType userType, int amount){
        basicRequests.put(userType, amount);
    }

    public void financialOperation(OperationType operationType, int amount){
        financialOperationRequests.put(operationType, amount);
    }

    public void processOrder(){
        if (!basicRequests.isEmpty()){
            loadFeederWithRequests(ServiceType.BASIC_SERVICE, basicRequests);
        }

        if (!financialOperationRequests.isEmpty()){
            loadFeederWithRequests(ServiceType.FINANCIAL_OPERATION, financialOperationRequests);
        }

        basicRequests.clear();
        financialOperationRequests.clear();

        feeder.connectToServer("127.0.0.1", 6666);
        feeder.beginTransference();
        System.out.println("Clients sendend");
    }

    private void loadFeederWithRequests(ServiceType serviceType, Map<Object, Integer> map){
        for (Object key:
                map.keySet()) {
            feeder.load(clientStorage.pollByFilter(serviceType, key, map.get(key)));
        }
    }
}
