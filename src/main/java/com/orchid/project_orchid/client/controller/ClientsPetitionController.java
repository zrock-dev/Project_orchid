package com.orchid.project_orchid.client.controller;

import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;

public class ClientsPetitionController {
    @FXML
    private TextField basicAccountHoldersTextField;

    @FXML
    private TextField withdrawAccountHoldersTextField;

    @FXML
    private TextField depositAccountHoldersTextField;

    @FXML
    private TextField loanPaymentAccountHoldersTextField;

    @FXML
    private TextField priorityUsersTextField;

    @FXML
    private TextField standardUsersTextField;

    @FXML
    private ToggleButton accountHoldersPetitionButton;

    @FXML
    private ToggleButton visitorsPetitionButton;

    private int amountStandard;
    private int amountPriority;
    private int amountBasicAccountHolder;
    private int amountAccountHolderDeposit;
    private int amountAccountHolderLoan;
    private int amountAccountHolderWithdraw;

    private Request request;

    public void setRequest(Request request) {
        this.request = request;
    }

    @FXML
    void sendAccountHoldersPetition(ActionEvent event) {

        if (!depositAccountHoldersTextField.getText().equals("")){
            amountAccountHolderDeposit = Integer.parseInt(depositAccountHoldersTextField.getText());
            request.financialOperation(OperationType.DEPOSIT, amountAccountHolderDeposit);
        }

        if (!withdrawAccountHoldersTextField.getText().equals("")){
            amountAccountHolderWithdraw = Integer.parseInt(withdrawAccountHoldersTextField.getText());
            request.financialOperation(OperationType.WITHDRAW, amountAccountHolderWithdraw);

        }

        if (!loanPaymentAccountHoldersTextField.getText().equals("")){
            amountAccountHolderLoan = Integer.parseInt(loanPaymentAccountHoldersTextField.getText());
            request.financialOperation(OperationType.LOAN_PAYMENT, amountAccountHolderLoan);
        }

        resetFinancialOperation();
        request.processOrder();
    }

    @FXML
    public void sendVisitorsPetition(ActionEvent event) {
        if (!standardUsersTextField.getText().equals("")){
            amountStandard = Integer.parseInt(standardUsersTextField.getText());
            request.users(UserType.STANDARD, amountStandard);
        }

        if (!priorityUsersTextField.getText().equals("")){
            amountPriority = Integer.parseInt(priorityUsersTextField.getText());
            request.users(UserType.PRIORITY, amountPriority);

        }

        if (!basicAccountHoldersTextField.getText().equals("")){
            amountBasicAccountHolder = Integer.parseInt(basicAccountHoldersTextField.getText());
            request.users(UserType.ACCOUNT_HOLDER, amountBasicAccountHolder);
        }

        resetVisitor();
        request.processOrder();
    }

    private void resetVisitor(){
        amountStandard = 0;
        standardUsersTextField.setText("");

        amountPriority = 0;
        priorityUsersTextField.setText("");

        amountBasicAccountHolder = 0;
        basicAccountHoldersTextField.setText("");
    }

    private void resetFinancialOperation(){
        depositAccountHoldersTextField.setText("");
        withdrawAccountHoldersTextField.setText("");
        loanPaymentAccountHoldersTextField.setText("");

        amountAccountHolderDeposit = 0;
        amountAccountHolderWithdraw = 0;
        amountAccountHolderLoan = 0;
    }
}
