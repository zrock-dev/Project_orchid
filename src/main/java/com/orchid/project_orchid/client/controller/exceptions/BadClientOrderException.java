package com.orchid.project_orchid.client.controller.exceptions;

public class BadClientOrderException extends RuntimeException{
    public BadClientOrderException(String message) {
        super(message);
    }
}
