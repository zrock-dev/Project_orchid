package com.orchid.project_orchid.client.controller;

import com.orchid.project_orchid.client.controller.exceptions.BadClientOrderException;
import com.orchid.project_orchid.user_profiles.clients.Client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class Feeder {
    private Socket clientSocket;
    private ObjectOutputStream out;
    private ArrayList<Client> clients;

    public void connectToServer(String ip, int port){
        try {
            clientSocket = new Socket(ip, port);
            out = new ObjectOutputStream(clientSocket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load(ArrayList<Client> requestLoad){
        if(clients == null){
            clients = new ArrayList<>();
        }
        clients.addAll(requestLoad);
    }

    public void beginTransference() {
        if (clients.isEmpty()){
            throw new BadClientOrderException("The set of clients is empty");
        }

        try {
            for (Client client:
                    clients) {
                out.writeObject(client);
                out.flush();
            }
        }catch (IOException e){
            e.printStackTrace();
        }

        stopConnection();
        clients.clear();
    }


    public void stopConnection(){
        try {
            out.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
