package com.orchid.project_orchid.client;

import com.orchid.project_orchid.client.controller.ClientsPetitionController;
import com.orchid.project_orchid.client.controller.Request;
import com.orchid.project_orchid.client.model.ClientGenerator;
import com.orchid.project_orchid.client.model.ClientStorage;
import com.orchid.project_orchid.utils.FileLoader;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class ClientApplication extends javafx.application.Application {
    @Override
    public void start(Stage primaryStage) throws IOException {
        ClientStorage clientStorage = new ClientStorage();
        ClientGenerator generator = new ClientGenerator(clientStorage);
        generator.fillStorage();
        Request request = new Request(clientStorage);

        FXMLLoader fxmlLoader = new FXMLLoader(FileLoader.getFile("client-side.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        ClientsPetitionController petitionController = fxmlLoader.getController();
        petitionController.setRequest(request);

        primaryStage.setTitle("Client App");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
