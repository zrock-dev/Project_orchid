package com.orchid.project_orchid.client.model;

import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.user_profiles.clients.VisitorClient;
import com.orchid.project_orchid.client.model.exceptions.OverloadedException;
import com.orchid.project_orchid.user_profiles.filters.BasicServices;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import java.util.concurrent.ThreadLocalRandom;

public class ClientGenerator {
    private final Encryptor encryptor;
    private final ClientStorage clientStorage;
    private final StringBuilder privateKey;

    public ClientGenerator(ClientStorage clientStorage) {
        this.clientStorage = clientStorage;
        encryptor = new Encryptor();
        privateKey = new StringBuilder();
    }

    public void fillStorage(){
        while (true){
            try {
                clientStorage.add(makeAClient());
            }catch (OverloadedException ignored){
                System.out.println(100_000_000 - clientStorage.size());
                break;
            }
        }
        System.out.println("Storage full " + clientStorage.size());
    }

    public Client makeAClient() {
        int clientDice = randomInt(1, 100);
        if (clientDice < 50) {
            return makeAnAccountHolder(makeServiceType());
        } else {
            return makeAVisitor();
        }
    }

    public VisitorClient makeAVisitor() {
        return new VisitorClient(
                makeUniqueID(),
                makeVisitorType(),
                makeBasicService());
    }

    public AccountHolderClient makeAnAccountHolder(ServiceType serviceType) {
        if (serviceType == ServiceType.FINANCIAL_OPERATION) {
            return new AccountHolderClient(
                    makeUniqueID(),
                    encryptor.makePassword(),
                    makeFinancialOperation(),
                    makeBalance(),
                    makeTransaction()
            );
        }else {
            return new AccountHolderClient(
                    makeUniqueID(),
                    encryptor.makePassword(),
                    makeBasicService()
            );
        }
    }

    private UserType makeVisitorType() {
        return UserType.values()[randomInt(0, 1)];
    }

    private ServiceType makeServiceType() {
        return ServiceType.values()[randomInt(0, ServiceType.values().length - 1)];
    }

    private OperationType makeFinancialOperation() {
        return OperationType.values()[randomInt(0, OperationType.values().length - 1)];
    }

    private BasicServices makeBasicService() {
        return BasicServices.values()[randomInt(0, BasicServices.values().length - 1)];
    }

    private String makeUniqueID() {
        privateKey.setLength(0);
        privateKey.append("-");
        privateKey.append(makeALetter());
        privateKey.append(makeALetter());
        privateKey.append(makeALetter());
        privateKey.append(makeANumber());
        privateKey.append(makeANumber());
        privateKey.append(makeANumber());
        return this.privateKey.toString();
    }

    private int makeBalance() {
        return randomInt(1, 500) * 100;
    }

    private int makeTransaction() {
        return randomInt(1, 250) * 100;
    }

    /***
     * Generates a character from the abecedary that can be either uppercase
     * or lowercase.
     * @return a character from the abc.
     */
    private char makeALetter() {
        return makeABoolean() ?
                (char) randomInt(65, 90) : (char) randomInt(97,122);
    }

    private int makeANumber() {
        return randomInt(0, 9);
    }

    private boolean makeABoolean(){
        return randomInt(0, 1) == 0;
    }

    private int randomInt(int minimum, int maximum) {
        return ThreadLocalRandom.current().nextInt(minimum, maximum + 1);
    }
}
