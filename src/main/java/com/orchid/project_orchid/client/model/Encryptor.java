package com.orchid.project_orchid.client.model;

public class Encryptor {
    private final String [] keysForClients = new String[2];
    private int count = 0;

    public String makePassword(){
        String keyOfTheClient;

        keysForClients[0] = "Okay";
        keysForClients[1] = "Coffee";

        if (count == 10){
            count = 0;
        }

        if (count < 8){
            keyOfTheClient = keysForClients[0];
        } else{
            keyOfTheClient = keysForClients[1];
        }
        count++;

        return keyOfTheClient;
    }
}
