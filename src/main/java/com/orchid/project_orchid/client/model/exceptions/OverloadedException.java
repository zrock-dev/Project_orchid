package com.orchid.project_orchid.client.model.exceptions;

public class OverloadedException extends RuntimeException{
    public OverloadedException(String message) {
        super(message);
    }
}
