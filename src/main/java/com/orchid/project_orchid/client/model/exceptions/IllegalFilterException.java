package com.orchid.project_orchid.client.model.exceptions;

/**
 * Thrown when a filter can't be found or hasn't been
 * created.
 * <p>
 * The filters are the enum classifications.
 */
public class IllegalFilterException extends RuntimeException{
    public IllegalFilterException(String message) {
        super(message);
    }
}
