package com.orchid.project_orchid.client.model;

import com.orchid.project_orchid.user_profiles.clients.AccountHolderClient;
import com.orchid.project_orchid.user_profiles.clients.Client;
import com.orchid.project_orchid.client.model.exceptions.IllegalFilterException;
import com.orchid.project_orchid.client.model.exceptions.OverloadedException;
import com.orchid.project_orchid.user_profiles.filters.OperationType;
import com.orchid.project_orchid.user_profiles.filters.ServiceType;
import com.orchid.project_orchid.user_profiles.filters.UserType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Random;

public class ClientStorage {
    private final int MAX_STORAGE_SIZE = 3_000_000;
    private final Random rand;

    private final LinkedHashMap<Object, ArrayList<Client>> basicServiceClients;
    private final LinkedHashMap<Object, ArrayList<Client>> financialOperationClients;
    private long storageSize;

    public ClientStorage(){
        rand = new Random();
        storageSize = 0;
        basicServiceClients = new LinkedHashMap<>(UserType.values().length);
        financialOperationClients = new LinkedHashMap<>(OperationType.values().length);

        populate(basicServiceClients, UserType.values());
        populate(financialOperationClients, OperationType.values());
    }

    /**
     * Populates the map with the initial filters, so it will be filling.
     * @param map The map to be populated with an enum classes as keys.
     * @param enumValues the enum values to be used as filters.
     */
    private void populate(LinkedHashMap<Object, ArrayList<Client>> map, Object[] enumValues){
        for (Object key:
             enumValues) {
            map.put(key, new ArrayList<>(MAX_STORAGE_SIZE/10));
        }
    }

    /**
     * This method adds clients to the storage, it filters the client according
     * to the kind of service required and saves it in the corresponding ArrayList.
     * @param client d
     */
    public void add(Client client){
        if (storageSize > MAX_STORAGE_SIZE){
            throw new OverloadedException("The storage capacity is full");
        }

        if (client.requiresBasicServices()){
            getListFromHash(basicServiceClients, client.getUserType()).add(client);

        }else {
            AccountHolderClient holderClient = null;
            try {
                holderClient = ((AccountHolderClient) client);
            }catch (ClassCastException e){
                e.printStackTrace();
            }

            getListFromHash(financialOperationClients, holderClient.getFinancialOperation()).add(client);
        }

        storageSize++;
    }

    /**
     * Returns the total size of the storage.
     * @return the size of the storage as integer.
     */
    public long size() {
        return storageSize;
    }

    /**
     * This method extracts the list in a hash, which keys represent the filters.
     * @param map The HashMap to extract the keys from.
     * @param filter The filter or key to get from the HashMap.
     * @return an ArrayList of clients under a specific filter.
     */
    private ArrayList<Client> getListFromHash(LinkedHashMap<?, ArrayList<Client>> map, Object filter) {
        return map.get(filter);
    }

    /**
     * @param bound max exclusive limit.
     * @return a random number between zero and the limit or bound.
     */
    private int randomInt(int bound) {
        return rand.nextInt(bound);
    }

    /**
     * This method filters the clients under a respective filter by extracting them from a
     * HashMap. Then it fills another list with the clients with the respective amount required.
     *
     * @param serviceType The kind of service the client needs.
     * @param filter The clients are associated under a filter.
     * @param clientsAmountRequired The amount of clients required under an amount.
     * @return An ArrayList of clients filtered and with a fixed size.
     */
    public ArrayList<Client> pollByFilter(ServiceType serviceType, Object filter, int clientsAmountRequired) {
        ArrayList<Client> filteredList = filterList(serviceType, filter);

        assert filteredList != null;
        if (clientsAmountRequired > filteredList.size()) {
            throw new IllegalStateException("The filter "+ filter+ " hasn't enough clients");
        }

        ArrayList<Client> requestedClients = new ArrayList<>(clientsAmountRequired);
        for (int i = 0; i < clientsAmountRequired; i++) {
            requestedClients.add(
                    filteredList.remove(randomInt(filteredList.size()))
            );
        }

        return requestedClients;
    }

    /**
     * This method picks the respective HashMap under a service type classification from
     * which the filtered clients can be retrieved.
     *
     * @param serviceType The kind of service a client is classified.
     * @param filter A filter to apply the filtering process.
     * @return An ArrayList with the filtered clients.
     */
    private ArrayList<Client> filterList(ServiceType serviceType, Object filter) {
        switch (serviceType) {
            case BASIC_SERVICE:
                isInList(filter, UserType.values());
                return getListFromHash(basicServiceClients, filter);

            case FINANCIAL_OPERATION:
                isInList(filter, OperationType.values());
                return getListFromHash(financialOperationClients, filter);
        }
        return null;
    }

    /**
     * This method verifies if an item is in a list.
     * @param item to find in the list.
     * @param list the list to iterate and find the object.
     * @throws IllegalFilterException When the filter hasn't been found in the list.
     */
    private void isInList(Object item, Object[] list) {
        boolean evaluation = false;
        for (Object occurrence :
                list) {
            if (occurrence.equals(item)) {
                evaluation = true;
                break;
            }
        }

        if (!evaluation) {
            throw new IllegalFilterException("The filter " + item +
                    " is not valid for the services:\n " + Arrays.toString(list));
        }
    }
}
