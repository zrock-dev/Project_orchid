package com.orchid.project_orchid.utils;

import com.orchid.project_orchid.Application;

import java.net.MalformedURLException;
import java.net.URL;

public class FileLoader {
    public static URL getFile(String pathToFile){
        URL url = Application.class.getResource(pathToFile);

        if (url == null){
            throw new NullPointerException();
        }

        return url;
    }
}
